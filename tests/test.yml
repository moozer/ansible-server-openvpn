---
# test provisioning corresponding to the Vagrantfile
- name: Setup VPN server
  hosts: server-vpn:localhost
  user: vagrant
  become: false
  become_method: sudo

  vars:
    - dhprimes_bitlength: 2048 # sufficient to check if it works.
    - int_if: { interface: ens7, ip: 10.10.10.5, netmask: 255.255.255.0 }
    - ext_if: { interface: ens6, ip: 10.10.0.5, netmask: 255.255.255.0 }
    - interfaces:
        - "{{ int_if }}"
        - "{{ ext_if }}"
    - openvpn_server_connect_name: 10.10.0.5

  roles:
    - { role: server_openvpn }

  pre_tasks:
    - name: setup apt
      become: true
      ansible.builtin.import_tasks: tasks/apt.yml

  tasks:
    - name: set ips
      become: true
      ansible.builtin.import_tasks: tasks/setupInterfaces.yml

  post_tasks:
    - name: "Test connectivity - external: {{ ext_if.interface }}"
      ansible.builtin.command: "ping -c 1 -I {{ ext_if.interface }} 10.10.0.200"
      changed_when: false
      register: ping_ext
      until: ping_ext.stdout.find("1 packets transmitted, 1 received") > -1
      retries: 15
      delay: 4
      tags: tests
    - name: "Test connectivity - internal: {{ int_if.interface }}"
      ansible.builtin.command: "ping -c 1 -I {{ int_if.interface }} 10.10.10.200"
      changed_when: false
      register: ping_int
      until: ping_int.stdout.find("1 packets transmitted, 1 received") > -1
      retries: 15
      delay: 4
      tags: tests

  handlers:
    - name: Flush interfaces
      become: true
      ansible.builtin.command: "ip addr flush {{ item }}"
      with_items: ["{{ int_if.interface }}", "{{ ext_if.interface }}"]
      when: not openvpn_just_install

    - name: Restart networking
      become: true
      ansible.builtin.service:
        name: networking
        state: restarted
      when: not openvpn_just_install

- name: Check external client
  hosts: server-ext
  user: vagrant
  become: false
  become_method: sudo

  vars:
    interface: ens6

  roles:
    - { role: server_openvpn, openvpn_include_server: false }

  pre_tasks:
    - name: setup apt
      become: true
      ansible.builtin.import_tasks: tasks/apt.yml

  tasks:
    - name: Output marker
      ansible.builtin.debug:
        msg: "external client"

  post_tasks:
    - name: "Test connectivity: {{ interface }}"
      ansible.builtin.command: "ping -c 1 -I {{ interface }} 10.10.0.5"
      changed_when: false
      register: ping_vpn
      until: ping_vpn.stdout.find("1 packets transmitted, 1 received") > -1
      retries: 15
      delay: 4
      tags: tests

    - name: Test connectivity through vpn (server)
      ansible.builtin.command: ping -c 1 -I tun0 10.10.10.5
      changed_when: false
      register: ping_vpn_tun
      until: ping_vpn_tun.stdout.find("1 packets transmitted, 1 received") > -1
      retries: 15
      delay: 4
      tags: tests

    - name: Test connectivity through vpn (server-int)
      ansible.builtin.command: ping -c 1 -I tun0 10.10.10.200
      changed_when: false
      register: ping_vpn_int
      until: ping_vpn_int.stdout.find("1 packets transmitted, 1 received") > -1
      retries: 15
      delay: 4
      tags: tests

- name: Check internal client
  hosts: server-int
  user: vagrant
  become: false
  become_method: sudo

  vars:
    interface: ens6

#  roles:
#  - { role: server_openvpn }

  tasks:
    - name: Output marker
      ansible.builtin.debug:
        msg: "internal client"

    - name: Check default route
      ansible.builtin.shell: "set -o pipefail && ip route show | grep default | grep 192.168.121"
      register: default_gw_check
      failed_when: false
      changed_when: false

    - name: Delete default gw through upstream
      become: true
      ansible.builtin.command: ip route del 0.0.0.0/0
      when: not default_gw_check.rc

    - name: Set up default gw through vpn server
      become: true
      ansible.builtin.command: ip route add 0.0.0.0/0 via 10.10.10.5
      when: not default_gw_check.rc

  post_tasks:
    - name: Test connectivity to vpn server
      ansible.builtin.command: "ping -c 1 -I {{ interface }} 10.10.10.5"
      changed_when: false
      register: ping_vpn
      until: ping_vpn.stdout.find("1 packets transmitted, 1 received") > -1
      retries: 15
      delay: 4
      tags: tests

    - name: Test connectivity - ext server
      ansible.builtin.command: "ping -c 1 -I {{ interface }} 10.10.0.5"
      changed_when: false
      register: ping_vpn
      until: ping_vpn.stdout.find("1 packets transmitted, 1 received") > -1
      retries: 15
      delay: 4
      tags: tests
